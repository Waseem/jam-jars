<?php

namespace AppBundle\Tests\Service;

use AppBundle\Service\JamJar as JamJarService;

class JamJarTest extends \PHPUnit_Framework_TestCase
{
	/**
	 * Test to make Two clones of JamJar entity instance
	 */
	public function testCloneJar()
	{
		$cloneCount = 3;

		// Entity mock expecting entity to be cloned twice
		$entityMock = $this->createOriginalEntityMock($cloneCount);

		// Entity manager mock to expect Persist function getting invoked twice!
		$entityManagerMock = $this->createEntityManagerMock($cloneCount);
		$serviceInstance = new JamJarService($entityManagerMock);

		// Invoking method to test, asking to make two clones of given entity (mock) instance
		// Since clones will be created for count > 1, we will increment desired count by 1 to match test expectations
		$serviceInstance->cloneJar($entityMock, $cloneCount + 1);
	}

	private function createEntityManagerMock($persistCount = 1)
	{
		$mock = $this->getMockBuilder('Doctrine\ORM\EntityManager')
			->disableOriginalConstructor()
			->getMock();

		// Persist function mock
		$mock->expects($this->exactly($persistCount))
			->method('persist');

		// Flush function mock
		$mock->expects($this->once())
			->method('flush');

		return $mock;
	}

	private function createOriginalEntityMock($cloneCount = 1)
	{
		$mock = $this->createEntityMock();

		$mock->expects($this->exactly($cloneCount))
			->method('getJamType');

		$mock->expects($this->exactly($cloneCount))
			->method('getProductionYear');

		$mock->expects($this->exactly($cloneCount))
			->method('getComment');

		return $mock;
	}

	private function createEntityMock()
	{
		return $this->getMockBuilder('AppBundle\Entity\JamJar')
			->getMock();
	}
}
