<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Jam Jar Entity
 *
 * @ORM\Table(name="jam_jar")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\JamJarRepository")
 */
class JamJar
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var JamType
     *
     * @ORM\ManyToOne(targetEntity="JamType")
     * @ORM\JoinColumn(name="jam_type_id", referencedColumnName="id")
     */
    private $jamType;

    /**
     * @var ProductionYear
     *
     * @ORM\ManyToOne(targetEntity="ProductionYear")
     * @ORM\JoinColumn(name="production_year_id", referencedColumnName="id")
     */
    private $productionYear;

    /**
     * @var string
     *
     * @ORM\Column(name="comment", type="text", nullable=true)
     */
    private $comment;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return JamType
     */
    public function getJamType()
    {
        return $this->jamType;
    }

    /**
     * @param JamType $jamType
     *
     * @return JamJar
     */
    public function setJamType($jamType)
    {
        $this->jamType = $jamType;

        return $this;
    }

    /**
     * @return ProductionYear
     */
    public function getProductionYear()
    {
        return $this->productionYear;
    }

    /**
     * @param ProductionYear $productionYear
     *
     * @return JamJar
     */
    public function setProductionYear($productionYear)
    {
        $this->productionYear = $productionYear;

        return $this;
    }

    /**
     * Set comment
     *
     * @param string $comment
     *
     * @return JamJar
     */
    public function setComment($comment)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment
     *
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Clones instance into new instance
     *
     * @return self
     */
    public function __clone()
    {
        $cloneInstance = new self;

        $cloneInstance
            ->setJamType( $this->getJamType() )
            ->setProductionYear( $this->getProductionYear() )
            ->setComment( $this->getComment() );

        return $cloneInstance;
    }
}

