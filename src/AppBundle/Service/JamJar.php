<?php

namespace AppBundle\Service;

use AppBundle\Entity\JamJar as JamJarEntity;
use Doctrine\ORM\EntityManager;

/**
 * Jam Jar Service
 *
 * @package AppBundle\Service
 */
class JamJar
{
	/**
	 * @var EntityManager
	 */
	private $entityManager;

	/**
	 * JamJar service constructor
	 *
	 * @param EntityManager $em
	 */
	public function __construct(EntityManager $em)
	{
		$this->entityManager = $em;
	}

	/**
	 * Clones given JamJar object stated number of times
	 *
	 * @param JamJarEntity $original
	 * @param int          $count
	 */
	public function cloneJar(JamJarEntity $original, $count)
	{
		$anyChange = false;

		while ($count > 1) {
			$clone = clone $original;

			$this->entityManager->persist($clone);

			$count--;

			if (!$anyChange) {
				$anyChange = true;
			}
		}

		if ($anyChange) {
			$this->entityManager->flush();
		}
	}
}