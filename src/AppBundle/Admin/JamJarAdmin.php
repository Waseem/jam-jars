<?php

namespace AppBundle\Admin;

use AppBundle\Entity\JamJar;
use AppBundle\Entity\JamType;
use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;

class JamJarAdmin extends Admin
{
	protected function configureFormFields(FormMapper $form)
	{
		$form
			->add('jamType', 'entity', [
				'class' => 'AppBundle\Entity\JamType',
				'property' => 'name'
			])
			->add('productionYear', 'entity', [
				'class' => 'AppBundle\Entity\ProductionYear',
				'property' => 'year'
			])
			->add('comment', 'textarea');

		/** @var JamJar $referencedEntity */
		$referencedEntity = $this->getSubject();

		// Is form used for Create?
		if (!$referencedEntity->getId()) {
			// Add field to capture number of copies of Jam-Jar entities to create
			$form->add(
				'amount',
				'number',
				[
					'data' => 1,
					'mapped' => false,
				]
			);
		}
	}

	protected function configureListFields(ListMapper $list)
	{
		$list
			->addIdentifier('id')
			->add('jamType.name')
			->add('productionYear.year')
			->add('comment');
	}

	/**
	 * Give UI a user-friendly name of the entity
	 *
	 * @param mixed $object
	 *
	 * @return string
	 */
	public function toString($object)
	{
		$strVal = 'Jam Jar';

		if ($object instanceof JamJar and $object->getJamType() instanceof JamType) {
			$strVal = $object->getJamType()->getName() . ' Jam Jar';

			$count = $this->getClonesCount();
			if ($count > 1) {
				$strVal = $count . ' x ' . $strVal . 's';
			}
		}

		return $strVal;
	}

	public function postPersist($object)
	{
		$count = $this->getClonesCount();

		/** @var \AppBundle\Service\JamJar $service */
		$service = $this->getConfigurationPool()->getContainer()->get('app.jamJar');

		$service->cloneJar($object, $count);
	}

	/**
	 * Gets count of clones to be created
	 *
	 * @return int
	 */
	protected function getClonesCount()
	{
		return (int) $this->getForm()->get('amount')->getData();
	}
}