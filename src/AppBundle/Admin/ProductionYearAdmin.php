<?php

namespace AppBundle\Admin;

use AppBundle\Entity\ProductionYear;
use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;

class ProductionYearAdmin extends Admin
{
	protected function configureFormFields(FormMapper $form)
	{
		$form->add('year', 'text');
	}

	protected function configureListFields(ListMapper $list)
	{
		$list
			->addIdentifier('id')
			->add('year');
	}

	/**
	 * Give UI a user-friendly name of the entity
	 *
	 * @param mixed $object
	 *
	 * @return string
	 */
	public function toString($object)
	{
		return ($object instanceof ProductionYear)
			? $object->getYear()
			: 'Production Year';
	}
}