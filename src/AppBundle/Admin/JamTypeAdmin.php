<?php

namespace AppBundle\Admin;

use AppBundle\Entity\JamType;
use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;

class JamTypeAdmin extends Admin
{
	protected function configureFormFields(FormMapper $form)
	{
		$form->add('name', 'text');
	}

	protected function configureListFields(ListMapper $list)
	{
		$list
			->addIdentifier('id')
			->add('name');
	}

	/**
	 * Give UI a user-friendly name of the entity
	 *
	 * @param mixed $object
	 *
	 * @return string
	 */
	public function toString($object)
	{
		return ($object instanceof JamType)
			? ($object->getName() . ' Jam')
			: 'Jam Type';
	}
}